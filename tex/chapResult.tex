\documentclass[../main.tex]{subfiles}
\begin{document}
\chapter{Results}
\label{chap:results}
This chapter shows how the online forward-backward algorithm behaves in practice, using the setup described in the previous chapter. If an exhaustive study has been undergone, only the significant, and good enough results are shown here.
\section{Choice of metrics}
If a qualitative visual study of the results of the different setup presented here can help to find good values for the parameters involved, it is also require to provide a quantitative analysis of the reconstruction, which ease the comparison between results and also with the litterature. In particular we rely here on two set of metrics: On the one hand we have reference metrics, where the results is compare to a reference image, for instance the ground truth image on \cref{fig:data:gt}. On the other there is cost metrics, evaluating the cost function with the estimate at each step , which provides insights on the behaviour of the algorithm.
\subsection{reference metrics}
Lets consider to real valued images \(\x\) and \(\x_{ref}\) of size \(N_{1}\times N_{2}\), where \(\x_{ref}\) is the reference image.
\subsubsection{Peak Signal-to-Noise Ratio (PSNR)}
PSNR is commonly used to quantify reconstruction quality for images and video subject to lossy compression, it is also well suited for studying compress sensing problem where only a part of the original data has been use to produce a  new estimate.
It is defined as follows:
\begin{equation}
  \label{eq:psnr_def}
  PSNR(\x,\x_{ref}) = 10 \log_{10}\left(\frac{\|\x_{ref}\|_{2}^{2}}{\|\x-\x_{ref}\|_{2}^{2}}\right)
\end{equation}

\subsubsection{Structural similarity (SSIM)}
In contrast to PSNR, The Structural Similarity index\cite{wang_image_2004-1} ains to provides a metric of similarity between two images.
Instead of making pixel-wise comparison (like in PSNR or MSE), the SSIM looks for difference of informations, contains in feature of the image (here the luminance, the contrast and the structure).
In our study the implementation provided by the framework scitkit-learn\footnote{\url{https://scikit-image.org/docs/dev/api/skimage.metrics.html}}, yields a value betwee  0 and 1, and a ssim of 1 is reached for identical images.


\subsection{cost metrics}

The online formulation of the reconstruction problem can be understand as a succession of solving smaller problem, hence, the cost function of the problem changes at each step (as the observation data changes).
To summarize this we define the following costs functions:

Let \((f_{k})_{k}\), \((g_{k})_{k}\) and \((\x_{k})_{k}\) being respectively sequence of  data cost function,  regularisation cost function and problem solution estimates. We set:
\begin{description}
  \item[Online Cost] \(c_{k} = f_{k}(\x_{k})+g_{k}(\x_{k})\) This cost describes the loss of the problem at step \(k\), it thus depends of the type of reconstruction use (type I or type II) and reflects what is the current local state of the algorithm.
  \item[Offline Cost] \(C_{k} = f(\x_{k})+g(\x_{k})\) The offline cost provides a global metrics of the altgorithm, by comparing the current estimation, obtained with partial data, with all the data that will be used in the end. In practice this metric will not be computable at the time of iteration, as the data is not yet fully acquired.
\end{description}

In the context of online reconstruction, this two cost function are relevant:
For type 1, they should converge towards the same value, as the last step of the type I reconstruction is equivalent to the offline one.
For type II each data cost function is different, but according to the work presented in \cref{chap:online} the offline cost should in the end be identical (up to margin of error) to an offline one.
\section{Offline references}

In order to quantify the advantage of the online reconstruction algorithm, we present here the state of the art of online reconstruction using forward-backward algorithm. The fastest method to compute an image is the zero-field reconstruction shown on \cref{fig:offline_ref:ifft} it consist only of a direct inversion, without any regularisation of the \(k\)-space data. When taking the regularisation into account, the quality is a little enhanced (a wider gap is to be expected with either fewer data or more noisy data) as on \cref{fig:offline_ref:pogm}. This effect is also a consequence of the multicoil acquisition and reconstruction of the data, which boost the SNR by producing more robust data.
\setlength{\mywidth}{0.33\textwidth}
\begin{figure}[hbt]
  \centering
  \subcaptionbox{Ground truth image \\ PSNR = \(+\infty\), SSIM = 1.000\label{fig:offline_ref:gt}}{\includegraphics[width=\mywidth]{ground_truth_square.png}}
  \hfill
  \subcaptionbox{Zero Field Reconstruction (direct IFFT)\\ PSNR = 32.38 dB, SSIM = 0.899\label{fig:offline_ref:ifft} }{\includegraphics[width=\mywidth]{offline_zero.png}}%
  \hfill
  \subcaptionbox{POGM with GroupLASSO (\(\omega=2\cdot10^{-6}\)), 200 iterations\\ PSNR=33.63 dB, SSIM=0.909\label{fig:offline_ref:pogm}}{\includegraphics[width=\mywidth]{offline_pogm.png}}%
  \hfill
  \caption{Reference images, offline reconstruction}
  \label{fig:offline_ref}
\end{figure}
\pagebreak
\section{Online type I}
\subsection{Convergence study}

\setlength{\mywidth}{0.9\linewidth}
\setlength{\myheight}{0.5\mywidth}

The convergence of the online reconstruction using a type I (accumulation of the observed data) is also verified in practice.
The plot of the online cost \(c_{k}\) on \cref{fig:costI} also give insight on the behaviour of the online reconstruction: we can see that the online cost reaches the same values as the offline one, but with a different path.
Moreover, the case of a growing slope in the last iteration of online demonstrate that the last iterations (done on the edge of the \(k\)-space, \ie high frequency) does not provide as much ``force'' to minimize the cost of the reconstruction.
This is confirmed by decomposing the online cost into its data consistency term and regularisation one on \cref{fig:cost_decompI}.
In the last quarter of the reconstruction almost all the cost is beared by the regularisation term.

To further accelerate  the reconstruction procedure, one could thus only choose to apply the regularisation step (which is expensive, due to a necessary round-trip in the wavelet domain) on the last steps of the reconstruction.

Moreover the performance of the vanilla online Forward-Backward are  enhanced by the acceleration technics of the FISTA and POGM algorithm.
\begin{figure}[h!]
  \centering
  \input{plot/cost_typeI.pgf}
  \caption[Online type I convergence]{Online Type I cost evolution. Lower is better. The last step of the online reconstruction are equivalent to the offline one. The offline reconstruction (dash-dotted line) start after the 80th iteration, when all the data has been acquired.}\label{fig:costI}
\end{figure}
\begin{figure}[h!]
  \centering
  \input{plot/data_res_typeI.pgf}
  \caption[Online type I convergence]{Decomposition of the online cost \(c_{k} = f_{k}(\x_{k})+g_{k}(\x_{k})\) for type  I reconstruction cost evolution.  }\label{fig:cost_decompI}
\end{figure}

\subsection{Reconstructions images}

The evolution of the quantitative metrics during the online reconstruction are shown on \cref{fig:metricI}. Here we can clearly see the advantage of it: The online reconstruction reach better metrics \emph{earlier} in time.
If the progress in psnr and ssim is slower than during an offline reconstruction, the type I online reconstruction reachs in the end a (slighlty) better results as presented by \cref{fig:online_fista} the cause of this effect could be the same as the one described for the cost evolution:
The last step of acquisition which are prone to noise degradation and very sensitive are only added in the reconstruction in the end, which minimized its effect on the final reconstruction, it also leaves more room for the regularisation step to express itself and be more effective.


\begin{figure}[h!]
  \centering
  \input{plot/metric_typeI.pgf}
  \caption[Online type I convergence]{Online Type I cost evolution. The last step of the online reconstruction are equivalent to the offline one. }\label{fig:metricI}
\end{figure}


\begin{figure}[h!]
  \centering
  \hfill
   \subcaptionbox{Best type I reconstruction, FISTA, default parameters\\ PSNR=33.67dB, SSIM=0.923}{
     \includegraphics[width=0.4\textwidth]{online_fistaI.png}}%
   \hfill
 \subcaptionbox{Reference image}{
   \includegraphics[width=0.4\textwidth]{ground_truth_square.png}}
 \hfill
\caption[Online Type I reconstruction]{Online Type I reconstruction, using FISTA algorithm, with default parameters. It provides slightly better results than the offline reconstruction of \cref{fig:offline_ref}.}
\label{fig:online_fista}
\end{figure}

\section{Online type II }

When the observed data is not accumulated anymore, and simply iterated the reconstruction behave differently.
Each new acquisition provides completly new information, and the regularisation also reduce the novelty effect of each new shot.
In practise this thus yields poor results, and the offline cost does not reach better results, as shown on \cref{fig:online_II_metrics}.
Moreover, if one consider a no regularisation reconstruction (\ie a simple gradient descent), the final reconstruction is better, and almost reaches the quality of the zero-field reconstruction.

The main advantage of type II lies in fact in its memory usage and ease of computation.
It is an order of magnitude faster to compute the masked Fourier transform, and it has a very  small memory footprint (only the current slice of observed data needs to be stored).

\begin{figure}[hbt]
  \centering
  \input{plot/metric_typeII.pgf}
  \caption[Online type II convergence]{Online Type II cost evolution. Applying the regularisation at each step is nefast for the reconstruction.}\label{fig:online_II_metrics}
\end{figure}

\subsection{Adding Memory}

\begin{figure}[hbt]
  \centering
  \input{plot/metric_typeII_momentum.pgf}
  \caption[Online type I convergence]{Online Type I cost evolution. The last step of the online reconstruction are equivalent to the offline one. }\label{fig:online_II_momentum}
\end{figure}


The poor quality of the type II reconstruction can be attributed to its missing capabilities to retains the previous data.
To add such memory of the descent direction, several preconditionner have been tested (presented in \cref{tab:grad_methods}).
Among them its the Momentum methods (or heavy ball) which showed convergent and better results than a raw (or \emph{vanilla}) Forward-Backward algorithm.

The momentum preconditioner only add a parameter \(\beta\) which is a \emph{forgetting factor}, with \(\beta < 1 \) the previous direction of descents are forgotten exponentially, with \(\beta=1\) nothing is forgotten, and with \(\beta > 1\) the direction of descent are reinforce at each step.
In practice a \(\beta\) very close to 1 has shown the best results.
Moreover, in the case of type II reconstruction, the energy of the reconstructed image is not necessarly preserved: Only the first iteration contains the center frequency of the k-space, and without normalisation it is possible to have a higher amplitude in the reconstructed values, which then biased the regularisation step (\eg the thresholding values are not relevant anymore).
To counteract this effect, the learning rate \(\eta\) is decreased to \(1/80 = 0.0125\), and with \(\beta\simeq 1\), the central frequency, which restrains the amplitude of the image is globally applied once.

with all of the above taken into account the results of \cref{fig:online_II_momentum} recenter the problem in the realm of possibilities:
Adding the momentum method does help for the reconstruction, but the choice of the forgetting factor \(\beta\) is very sensible numerically. More tweaking of the method is surely required to provide even better reconstruction than the one shown on \cref{fig:online_II}.

\begin{figure}[h!]
  \centering
  \hfill
   \subcaptionbox{Type II, Forward-Backward(\(\eta=1.\) \& GroupLASSO \\ PSNR=23.35dB, SSIM=0.608}{
     \includegraphics[width=0.4\textwidth]{online_vanillaII_GL1.png}}%
   \hfill
 \subcaptionbox{Type II, Forward-Backward\(\eta=1\)\& no regularisation\\ PSNR=29.37dB, SSIM=0.890}{
   \includegraphics[width=0.4\textwidth]{online_vanillaII_Id1.png}}
 \hfill \\
 \hfill
  \subcaptionbox{Type II, Momentum \& GroupLASSO \\ PSNR=26.46, SSIM=0.839}{
    \includegraphics[width=0.4\textwidth]{online_momentumII.png}}%
  \hfill
 \subcaptionbox{Reference image}{
   \includegraphics[width=0.4\textwidth]{ground_truth_square.png}}
 \hfill
\caption[Online Type I reconstruction]{Online Type II reconstruction, using the Forward-Backward algorithm., with default parameters. It performs worse than the type I reconstruction}
\label{fig:online_II}
\end{figure}

\section{Computational Cost and efficiency}


\begin{table}[hbt]
  \centering
  \begin{tabular}{rlccccl}
    \toprule
    Method                     & Algorithm   & PSNR (dB)      & SSIM           & iter. (s) & total      & Memory usage    \\
    \midrule
    \multirow{2}{*}{Offline}   & IFFT        & 32.38          & 0.899          & \(\emptyset\)     & TA         & High            \\
                               & POGM        & 33.63          & 0.909          & 1.13      & 02:31 + TA & Highest         \\
    \midrule
    \multirow{2}{*}{Online I}  & FISTA       & \textbf{33.63} & \textbf{0.923} & 0.90      & 01:12      & Higher (at end) \\
                               & FB          & 32.54          & 0.903          & 0.80      & 01:08      & High (at end)   \\
    \midrule
    \multirow{2}{*}{Online II} & FB          & 23.35          & 0.608          & 0.27      & 00:22      & Low             \\
                               & FB (no reg) & 29.37          & 0.890          & 0.03      & 00:03      & Lowest          \\
                               & Momentum    & 26.46          & 0.839          & 0.28      & 00:23      & Lower           \\
    \bottomrule
  \end{tabular}
  \caption{Characteristics of the online reconstruction algorithms. TA: Acquisition time, depends of the mode of MRI acquisition (T1, T2, etc...), An sequence of acquisition last between 0.05 and 2s, so TA \(\simeq 2\) minutes in this particular case.}
  \label{tab:online_char}
\end{table}


Aside from the raw quality of the various reconstruction, it is wise to also consider their effiency. In this domain the online reconstruction looks very promising in particular the type II reconstruction.

The different results on methods tested so far are gathered on \cref{tab:online_char}, and show that the Online type I has the best reconstruction quality for an reasonable computational usage (which will be at it maximum equivalent to an offline setup). The Online type II present an interest for small configuration, and could be considered for embedded or integrated reconstruction on the MRI device, with its low memory footprint and its standart quality, such method could be use for internal calibration or test settings. It is  10 to 20 times faster to reconstruct with a type II method than with an offline method. Faster Computation could be reach with dedicated integration (for instance using GPUs or FPGAs), and have a so called ``real-time'' reconstruction.



\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "english"
%%% End:
