\documentclass[../main.tex]{subfiles}
\begin{document}
\chapter{The MRI Reconstruction problem}%
\label{chap:mri_reco}
This chapter introduced the key principles of MR Imaging and the methods use for reconstructing image from the acquired data.


Magnetic Resonance Imaging (MRI) is a non invasive technique to explore the human body with a wide range of contrasts.
Yet, the processus of acquistion is not trivial, and the goal of this chapter is to present the key principles of MRI.
At its core, the information represented in a MR Image is collected in the spatial fourier space, the k-space.
The sampling method of the k-space will determine with which input data the reconstruction to the image space will be done.
The larger the number of point sampled in the kspace, the better the reconstruction will be, but also the longer the acquisition will last. To reduced the acquisition time, two main approach have been develop: the compress sensing and the parallel imaging, and can be combined for a compound effect. However both methods require more complex reconstruction, at the expense of a delayed result.


\section{Base principles of MRI  signals acquisition}
\label{sec:MRI_brief}

The MR Imaging technique is based on the principe of Nuclear MAgnetic Resonance (NMR): the subject is placed in an intense constant and homogeneous magnetic field.
The hydrogenes protons in the subject aligns there protons spin and are thus parallel to  the polarising field.
Then a Radio Frequency pulse is applied, perpendiculary to the polarising field.
If the frequency of the pulse is the same than the spin frequency, the spin changes direction and thus leave their equilibrium state.
When the pulse stop, the spins returns to their equilibrium state, aligned with the polarising field.
During this phase, call relaxation, Energy releases from the spin is measure through antennas.
The time taken to return to equilibrium and the amount of energy release can then be used to determine magnetic properties of the tissues.


In order to spatialize this information, the polarising field is modified using gradients coils.
These gradients coils will encode the position of the measurement by shifting the excitation characteristics in frequency and phase. This provides spatial-frequency informations, which is gathered in the \(k\)-space.
The sampling pattern of the \(k\)-space and the reconstruction method used after it will thus determined the reconstruction image produced in the end. More information on the acquisition sequences and a (brief) introduction to reconstruction of MRI images can be found in~\cite{bernstein_handbook_2004}.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.7\textwidth]{mri_schematics.png}
  \caption{\label{fig:mri_Scheme} MRI components Representation. Having of strong homogeneous magnetic field all along the patient, requires to have huge solenoid circuits and  proper cooling.}
\end{figure}


\subsection{Acceleration  of acquisition methods}

According to Shannon's Theorem, the more points are acquired in the \(k\)-space, the higher the resolution of the reconstruction image will be. However this severly increases the examination time, as one as to wait between each shot that all the spin realign with the polarising field before applying the next pulse. To reduce the acquisition time, two main methods have been developed in the last two decade to reduce the acquisition time: parallel imaging, when multiples antennas acquires RF signal simultaneously, and compress sensing, where the k-space is undersampled, but uses a \emph{clever} way of reconstruction (which is also more complex).

\subsubsection{Parallel imaging}
Parallel imaging\cite{hamilton_recent_2017} allows for faster scans using multiple radio frequency coils, also called phased array coils. By using multiple coils (which are also smaller) one can for a constant SNR reduce the scan time or increase the acquired resolution. Each coil of an array has a particular sensitivity profile, which depend on various factor such has its position, the gap between it and the body etc...

Undersampling by a factor \(R\) a \(k\)-space for a particular Field of View (FOV) produces aliasing (frequency folding) artifacts.
However as the data is acquired on multiples coils with different sensitivity the information can be unfold, either in the frequency space (SMASH\cite{sodickson_simultaneous_1997}) or the image space (SENSE\cite{pruessmann_sense_1999}), nonetheless, these method requires first an estimation of the sensitivity map of each coils, done in a calibration step, as a first step of the examination.

A calibrationless acquisition and reconstruction is also possible, here each coils produced its own image, which are later combined using the square root of the sum of square (SSOS). This method is easier for the acquisition but requires more memory and computation during the reconstruction.

For its simplicity and its faster acquisition times we rely hereafter thus on a calibrationless settings (buying a bigger computer is easier than increasing the number of hours in a day).

\subsubsection{Compress sensing}

Compress sensing\cite{foucart_mathematical_2013} has been a major leap forward for signal and image processing, and is more and more used in the MRI community\cite{lustig_compressed_2008}:
By making a priori assumption on the characteristic of the signal, it is possible to remove the constraint of Shannon's Theorem.
Typically one can make a sparsity hypothesis in a specified basis (\eg after a wavelet transform).
This sparsity constraint is ideally enforce by a \(\ell_{0}\) norm, but for ease of computation, this is relaxed into a \(\ell_{1}\) norm regularisation step.
Other norm-like operation can be used to enforce more structural information, like total variation minimization, or GroupLASSO regularisation.
The case of GroupLASSO is particularly interessing in the case of parallel imaging, as the structural information can then be spread along the different per-coil acquired signals.

If the used of compress sensing can greatly accelerate the acquisition of data in the \(k\)-space (especially by using non-cartesian sampling, the ideal case being a fully random sampling, which is unreachable in practice due to hardware constraint). For instance the SPARKLING trajectories\cite{lazarus_sparkling_2019}, patented at Neurospin can provide a 4 to 5 fold acceleration factor on acquisition without compromises on the quality of the reconstruction.


\section{Inverse Problem solving}

\paragraph{Notation}
\begin{itemize}

\item Let \(N\) be the number of pixel in the image per channel, the size of image is thus \(n \times n = N\)
\item Let \(L\) be the number of channels used to acquire the NMR signal
\item let \(M\) be the number of measurements in the Fourier domain with \(M < N\)
  \item Vectors are in bold font \(\x \in \mathbb{C}^{N}\)
  \item Stack of vectors are in bold font and underlined \(\ubm{x}= [\x_{1} \dots{} \x_{L}] \in \mathbb{C}^{L\times N}\)
  \item Linear operators on vectors are in bold capital letter \(\bm{M} \in \mathcal{L}(\mathbb{C}^{N},\mathbb{C}^{N})\) and underlined \(\ubm{M} \in \mathcal{L}(\mathbb{C}^{L\times N},\mathbb{C}^{L\times N})\) if applied to a stack of vectors.
\end{itemize}


We denote by \(\ubm{y} \in \mathbb{C}^{L \times M }\) the acquired NMR signal and by \(\y_{\ell}\in \mathbb{C}^{M}\) the \(\ell\)-th channel observed data.
Let \(\ubm{x} = \left[ \x_1 \dots{} \x_{L}\right] \in \mathbb{C}^{L \times N}\) the reconstructed NMR image, such that \(\x_{\ell}\) is associated with the \(\ell\)-th channel.

The image reconstruction problem reads as follows, called the \emph{analysis problem}:

\begin{equation}
  \label{eq:analysis}
    \hat{\ubm{x}} = \argmin_{\ubm{x}\in \mathbb{C}^{L\times N}} \left\{ \frac12 \sum_{\ell=1}^{L} \left\| \F_\Omega (\x_{l}) -\y_{\ell} \right\|_2^2 + R(\ubm{\Psi}\,\ubm{x})\right\}
\end{equation}
where in the context of Cartesian data acquisition the undersampled Fourier operator reads:\(\F_\Omega= \bm\Omega\F\), with $\bm\Omega \in {\{0,1\}}^{M \times N}$ the undersampling mask ($M \leq N$) and $\F \in \mathbb{C}^{N \times N} $ the fast Fourier transform in the suitable domain (i.e., 2D Fourier transform when dealing with 2D images, for e.g.).
\(\ubm\Psi \in \mathbb{C}^{(L \times N_{\Psi})\times (L \times N) }\) is a linear sparsifying operator (eg a Wavelet transform).
In practice, the transform is the same for all coils and \(\ubm\Psi = \mathop{diag}(\bm\Psi_{1}\dots{}\bm\Psi_{L})\) is a bloc diagonal matrix, applying the transform to each coil, in parallel.
\(R \in \Gamma_{0}(\mathbb{C}^{L\times N_{\Psi}})\) is the regularization function, promoting sparsity of the wavelet coefficient across coils. $R$ is a convex, proximal-friendly function.


Conversely one can solve the \emph{synthesis problem}:
\begin{equation}
\label{eq:synthesis}
    \hat{\ubm{\alpha}} = \argmin_{\ubm{\alpha}\in \mathbb{C}^{L \times N_{\Psi}}} \left\{\frac12  \sum_{\ell=1}^{L} \left\| \F_\Omega \bm \Psi_{\ell}^*\bm\alpha_{\ell} - \y_{\ell} \right\|_2^2 + g(\ubm\alpha)\right\} \text{ and } \hat{\ubm{x}}= \ubm\Psi^* \hat{\ubm\alpha},
\end{equation}

Formulations~\eqref{eq:analysis} and~\eqref{eq:synthesis}  are convex but non-smooth optimization problems. If well studied in the literature, solving them remains time-consuming and non trivial, especially when considering large dimension (\ie{} higher resolution images): Usually, the problem is solved using iterative algorithms such as proximal gradient methods~(POGM\cite{kim_adaptive_2018}, FISTA\cite{beck_fast_2009}, etc.) or primal-dual techniques~(\eg{} Condat-Vu\cite{vu_splitting_2011}).

\begin{figure}[H]
  \centering
  \footnotesize{}
    \includegraphics[width=0.9\textwidth]{iterative_PI.png}
    \caption{\label{fig:pmri_principle}Principle of Calibrationless pMRI for $L$ coils, the final image is the square root of the sum of square (SSOS) of the per-channel image. }
\end{figure}

\subsection{Regularization via GroupLASSO}
\label{sec:GroupLASSO}
% Currently two regularization functions have been used in both analysis and synthesis paradigm: GroupLASSO (\ie{} social sparsity) and ordered weighted $\ell_1$-norm~(OWL).

The regularization is performed on the sparse representation of $\ubm{x}$, here the wavelets coefficients computed for each coil: \( \ubm\alpha \in \mathbb{C}^{L \times N_\Psi}\).

% \subsection{GroupLasso}

The GroupLASSO regularisation~\cite{yuan_model_2006} can be interpreted as a mixed \(\ell_{1,2}\) norm on the regularised variable (in our case the wavelets coefficients). Its proximal operator is thus a soft threshold operator, applied on a \emph{group} dimension (here the coils channels).
For a given \(\ubm{\alpha} \in \mathbb{C}^{L \times N_{\Psi}}\)  we have:

\begin{equation}
  \label{eq:GroupLASSO}
R_{GL} :
  \begin{bmatrix}
          \bm\alpha_{1}\\
          \vdots \\
          \bm\alpha_{L}
        \end{bmatrix}
  \mapsto \sum_{j=1}^{N_{\Psi}} w_{j} \sqrt{\sum_{\ell=1}^L |\alpha_{\ell,j}|^{2} }
\end{equation}

\begin{equation}
  \label{eq:proxGroupLASSO}
  \prox_{R_{GL}} : \begin{array}{l}
        \mathbb{C}^{L \times N_{\Psi}} \to  \mathbb{C}^{L \times N_{\Psi}}\\
        \begin{bmatrix}
          \bm\alpha_{1}\\
          \vdots \\
          \bm\alpha_{L}
        \end{bmatrix}
        \mapsto
        \begin{bmatrix}
          \bm\alpha'_{1}\\
          \vdots \\
          \bm\alpha'_{L}
        \end{bmatrix}
      \end{array}
\qquad \text{with}\qquad \alpha'_{\ell,j} =
\begin{cases}
  0 & \text{ if } \sqrt{\sum_{j=1}^{N_{\Psi}}\|\alpha_{\ell,j}\|_{2}^{2}} < w_{j} \\
  \alpha_{\ell,j} & \text{ else }
\end{cases}
\end{equation}
$(w_{j})$ are regularization weights, usually identical for every \(j \in \{1,\dots,N_{\Psi}\}\).

\paragraph{Remarks}
\begin{itemize}
  \item The GroupLASSO regularisation does not take the locality of the coefficients into account, this can be achieved using three-level norm, to develop a social sparsity~\cite{kowalski_social_2013}.
  \item In the case of a single coil reconstruction the GroupLASSO is simplified into a LASSO/Basis pursuit regularisation (simple \(\ell_{1}\) regularisation) and the proximal operator is then the classical soft thresholding operator.
\end{itemize}

% \subsection{OWL}
% The ordered weighted \( \ell_{1}\) norm~\cite{zeng_ordered_2015} is denoted as:
% \begin{equation}
%   \label{eq:OWL}
%   R_{OWL}:
%   \begin{array}{l}
%     \mathbb{C}^{N_{\Psi}} \to \mathbb{R} \\
%     \bm{\alpha} \mapsto R_{OWL}( \bm{\alpha}) = \sum_{i=1}^{n}|\alpha|_{[i]} w_{i}= {\bm{w}}^{T}|\bm{\alpha}|_{\downarrow}
%   \end{array}
% \end{equation}
% where \(\alpha_{[i]}\) is the \(i\)-th largest component of \(\bm{\alpha}\) and \(|\bm{\alpha}|_{\downarrow}\) is the vector obtained by sorting (by decreasing order) the component of \( \bm{\alpha}\), and \(\bm{w} \in \mathcal{K}_{m+} = \{ \bm{w} \in \mathbb{R}^{n}: w_{1} \geq w_{2} \geq \cdots w_{n} \geq 0\} \subset \mathbb{R}^{n}_{+}\)

% for \(\lambda_{1}, \lambda_{2} \ge 0 \) and \(w_{i} = \lambda_{1} + \lambda_{2}(N_{\Psi}-i)\), the OWL norm is the so-called OSCAR regularizer~\cite{bondell_simultaneous_2008}:
% \begin{equation}
%   \label{eq:OSCAR}
% \Omega_{\bm{w}}( \bm{\alpha})=\lambda_{1}\| \bm{\alpha}\|_{1}+\lambda_{2} \sum_{i<j} \max
% \left\{\left|\alpha_{i}\right|,\left|\alpha_{j}\right|\right\}
% \end{equation}

% The proximal operator is defined as:
% \begin{equation}
% \operatorname{prox}_{\Omega_{w}}( \bm{\alpha})=\operatorname{sign}( \bm{\alpha}) \odot\left(P{(| \bm{\alpha}|)}^{T} \operatorname{proj}_{\mathbb{R}_{+}^{n}}\left(\operatorname{proj}_{\mathcal{K}_{m}}\left(| \bm{\alpha}|_{\downarrow}-\bm{w}\right)\right)\right)
% \end{equation}
% Where \( P\) is the permutation matrix such that \(| \bm{\alpha}|_{\downarrow} = P( \bm{\alpha}) \bm{\alpha}\)

\subsection{Iteratives reconstruction methods}

The reconstruction problem stated in~\eqref{eq:analysis} or~\eqref{eq:synthesis} can not be solve directly, \ie{} there exists no closed form of the solution \(\ubm{x}\).
In particular the regularisation function \(g\) is not differentiable.
However a minimum of the cost function can still be found by using an iterative algorithm.
Taking advantage of the convexity of \(f+g\) is a key aspect of these methods, with a wide variety of example in the literature, like in~\cite{bauschke_convex_2011} and reference therein.
Moreover, the convexity, yet non differentiabilty of the regularisation function can be used along with the so-call proximity operator\cite{combettes_proximal_2010}, at heart of several iterative algorithms, such as the \emph{forward-backward algorithm}, described for instance by Combettes and Pesquet\cite{combettes_proximal_2010}, and two accelerated variants, namely \emph{FISTA}\cite{beck_fast_2009} and \emph{POGM}\cite{kim_adaptive_2018}.

The next chapter focuses on the Forward-Backward algorithm to present the framework of online reconstruction, a primal-dual study (for instance based on the Condat-Vù algorithm\cite{condat_primaldual_2013, vu_splitting_2011}) for this setup is still open to propositions.

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "english"
%%% End:
