\documentclass[../main.tex]{subfiles}
\begin{document}
\newcommand{\KK}{1,\dots,K}

\chapter{Online Reconstruction Methods}\label{chap:online}

The previous chapter introduced the concept of online reconstruction. Unlike traditionnal iterative methods presented in \cref{chap:mri_reco}, online reconstruction start simutaneoulsy with the acquistion of the observation data. Each new shot of acquisition is then fed in to the reconstruction algorithm incrementally. This approach can reduce the memory and computational cost of the reconstruction, while giving preliminary results earlier than the offline method, which start its reconstruction only after the acquisition is complete.
In either case, the reconstruction boils down to finding a solution of a classic optimisation problem:
\begin{equation}
  \label{eq:base_pb}
\hat{\x} = \argmin_{x\in\mathcal{H}} f(\x) + g(\x)
\end{equation}
Where \(\mathcal{H}\) is an euclidian space of finite dimension, \(f\) is a convex, differentiable function, modeling the data consistency term, and \(g\) is a convex, proximal-friendly function, modeling a regularisation term applied to \(\x\) or its representation after a linear transform (\eg{} Wavelet Transform).
As \(g\) is not differentiable, no closed form of \(\hat{\x}\) can be determined analytically, and an iterative estimation is necessary.
Among the variety of algorithms used in the literature, the Forward-Backward algorithm~\cite{combettes_proximal_2010} is a general algorithm superseeding different implentations like the well-known (F)ISTA \cite{beck_fast_2009}.
Such algorithm can be studied both from a fixed point operator point of vue, and a variational approach, giving results on  existence and convergence conditions.

In  \cref{sec:intro_fixed_point} we recall the main properties on the Forward-Backward Algorithm and their relations with fixed point operators. From these properties,  \cref{sec:fixed_point_algo} show that upon certain conditions, reproduced from~\cite{combettes_fixed_2021}, an online algorithm provides the same results as an offline one for problem~\eqref{eq:base_pb}.


\section{Introduction to Fixed Point Algorithms}\label{sec:intro_fixed_point}
\subsection{Recall on Fixed Point theory}\label{sec:prop_fixed_point}

The following Definitions, Propositions and Theorems are extracted from~\cite{combettes_fixed_2021} and references therein. We consider \(\mathcal{H}\) an Euclidian space of finite dimension, with associated norm \(\|~\cdot~\|\). \(\Gamma_{0}(\mathcal{H})\) denote the class of functions which are proper, lower semicontinous and convex.

\begin{Definition}[(Definition 9 of~\cite{combettes_fixed_2021})]\label{def:seq_relax}
  Let \( 0< \alpha \le 1 \), an \(\alpha\)-relaxation sequence is a sequence \({\left(\lambda_{n}\right)}_{n \in \mathbb{N}} \text { with }  0 < \lambda_{n} < 1 / \alpha\), such that \(\sum_{n \in \mathbb{N}} \lambda_{n}\left(1-\alpha \lambda_{n}\right)=+\infty\)
\end{Definition}
\begin{Definition}[Sub-differential]
  The \emph{subdifferential} of a proper function \(f : \mathcal{H} \to ]-\infty, +\infty]\) is the set valued operator
  \begin{equation*}
   \bm \partial f:  \begin{cases}
      \mathcal{H} &\to 2^{\mathcal{H}}\\
      \x & \mapsto \{\bm{u} \in \mathcal{H} ~|~ \forall \bm{y}\in\mathcal{H}, \langle\y-\x \rangle + f(\x) \le f(\y)\}
\end{cases}
\end{equation*}
\end{Definition}
\begin{Definition}[Characteristic of Operators]\label{def:operator}
  Let an  operator \(\bm{T}: \mathcal{H} \rightarrow \mathcal{H} \). We have the following definitions:

  \begin{enumerate}
    \item \(\bm{T}\) is \(\delta\)-\emph{lipschitzian} with \(\delta > 0 \) if
          \[
          (\forall \x \in \mathcal{H})(\forall \y \in \mathcal{H}) \quad\|\bm{T} \x-\bm{T} \y\| \le \delta\|\x-\y\| .
          \]
    \item \(\bm{T}\) is a \emph{Banach contraction} if \(\delta < 1\)
    \item \(\bm{T}\) is \emph{nonexpansive} if \(\delta =1\)
    \item \(\bm{T}\) is \emph{firmly nonexpansive} if
          \[(\forall \x \in \mathcal{H})(\forall \y \in \mathcal{H}) \|\bm{T} \x-\bm{T} \y\|^{2} \le \|\x-\y\|^{2}  \quad-\|(\mathrm{Id}-\bm{T}) \x-(\mathrm{Id}-\bm{T}) \y\|^{2}\]
    \item \(\bm{T}\) is \emph{cocoercive} with constant \(\beta > 0\) if
          \[
(\forall \x \in \mathcal{H})(\forall \y \in \mathcal{H}) \quad\langle \x-\y \mid \bm{T} \x-\bm{T} \y\rangle \ge \\ \beta\|\bm{T} \x-\bm{T} \y\|^{2}
          \]
    \item For \(0 < \alpha < 1\), if there exists a nonexpansive operator \(Q: \mathcal{H} \rightarrow \mathcal{H}\),
          such that \(\bm{T}\) can be written as:
          \[\bm{T}=\mathrm{Id}+\alpha(Q-\mathrm{Id})\]
          Then \(\bm{T}\) is a \emph{\(\alpha\)-averaged operator}
  \end{enumerate}
  Note that:
  \begin{align*}
    \label{eq:operator_eqv}
    \bm{T} \text{is firmly nonexpansive} & \iff  Id - \bm{T} \text{ is firmly nonexpansive}\\
                                    & \iff \bm{T} \text{ is  } 1/2-\text{averaged}\\
                                    & \iff \bm{T} \text{ is } 1-cocoercive\\
  \end{align*}
\end{Definition}

\begin{Definition}
  The proximal operator of a function of class \(\Gamma_{0}(\mathcal{H})\), is defined as:
  \begin{equation*}
    \label{eq:prox_def}
    \prox_{f} (\x) = \argmin_{\y \in \mathcal{H}} f(\y) + \frac12 \|\x-\y\|_{2}^{2}
  \end{equation*}

\end{Definition}

\begin{Proposition}[Example 11 of~\cite{combettes_fixed_2021}]\label{prop:prox-averaged}
  Let \(g \in \Gamma_{0}(\mathcal{H})\), then \(\prox_{g}\) and \(Id-\prox_{g}\) are firmly non expansive.
\end{Proposition}

\begin{Proposition}[Baillon-Haddad]\label{prop:baillon-haddad}
  Let \(f: \mathcal{H} \to \mathbb{R}\) a convex differentiable function such that \(\nabla f\) is \(\beta^{-1}\)-lipschitzian for some \(\beta > 0 \), then \(\nabla f\) is \(\beta\)-cocoercive.
\end{Proposition}

\begin{Proposition}\label{prop:cocoercive-averaged}
  Let \(\beta > 0\) and \( 0 < \gamma < 2\beta\). Then:
  \(\bm{T}\) is \(\beta\)-cocoercive if and only if \(Id-\gamma \bm{T}\) is \(\gamma/(2\beta)\)-averaged.
\end{Proposition}

\begin{Proposition}[Proposition 18 and 26 of~\cite{combettes_fixed_2021}]\label{prop:comp_operator}
  For every \(k \in 1, \dots, K\), let \( \alpha_{k} \in ]0 ,1[ \) and \(\bm{T}_k: \mathcal{H}\to\mathcal{H}\) be \(\alpha_k\)-averaged. Set
  \begin{equation*}
    \bm{T}=\bm{T}_{1} \circ \cdots \circ \bm{T}_{K} \text { and } \alpha=\frac{1}{1+\frac{1}{\sum_{k=1}^{K} \frac{\alpha_k}{1-\alpha_k}}}
  \end{equation*}
  Furthermore, suppose that \(\bigcap_{k=1}^{K} \Fix \bm{T}_k \neq \varnothing\). Then
  \[\Fix\bm{T} =\bigcap_{k=1}^{K} \Fix \bm{T}_k\]
\end{Proposition}
\begin{Corollary}\label{corol:comp}
  Let \(\bm{T}_{2}\) and \(\bm{T}_{2}\) two operators being respectively \(\alpha_1\) and \(\alpha_{2}\)-averaged. Then
  For \(\bm{T} = \bm{T}_{1}\circ \bm{T}_{2}\)  is \(\alpha\)-averaged with:
  \[\alpha = \frac{\alpha_{1}+\alpha_{2}-2 \alpha_{1} \alpha_{2}}{1-\alpha_{1} \alpha_{2}}\]
\end{Corollary}

\begin{Theorem}[\cite{bauschke_convex_2011}]\label{thm:bauschke}
  Let \(\alpha \in  ]0,1[\), let \(\bm{T}:\mathcal{H}\to \mathcal{H}\) be an \(\alpha\)-averaged operator such that \(\Fix \bm{T} \neq \emptyset\). Let \({(\lambda_{n})}_{n}\) be an \(\alpha\)-relaxation sequence. Set \(\x^{(0)} \in \mathcal{H}\) and:
  \begin{equation}
  (\forall n \in \mathbb{N}) \quad x^{(n+1)}=x^{(n)}+\lambda_{n}\left(\bm{T} x^{(n)}-x^{(n)}\right)
\end{equation}
Then \({(x^{(n)})}_{n}\) converges to a point in \(\Fix \bm{T}\)
\end{Theorem}

\subsection{Fixed point formalism for the forward-backward algorithm}
\subsubsection{Offline Forward-Backward}
To solve problem~\eqref{eq:base_pb}, the offline Forward-Backward reconstruction algorithm is commonly used. It can be described as follows:

\begin{equation}
  \label{eq:offline_fb}
 (\forall n \in \mathbb{N} ),\quad \x^{(n+1)} = \prox_{\eta g}\left(\x^{(n)}-\eta \nabla f(\x^{(n)})\right)
\end{equation}
It can be expressed as a fixed point algorithm as follows:

\begin{equation}
  \label{eq:offline_fb_fixed_point}
  (\forall n \in \mathbb{N}),\quad \x^{(n+1)} = \bm{T}\x^{(n)}
\end{equation}
Where \(\bm{T}\) is the fixed point operator of the offline algorithm:
\begin{equation}\label{eq:T_offline}
\bm{T} =  \prox_{\eta g}\left(Id-\eta\nabla f\right)
\end{equation}

\subsubsection{Split online Forward-Backward}
\begin{Assumption}\label{ass:split}
Let \(K\in \mathbb{N}\). 

Let \(f,g \in \Gamma_{0}(\mathcal{H})\)
Let \(\nu_{1},\dots, \nu_{K} \in \mathbb{R}_{+}^{K}\)
Suppose there exists \(f_{1},\dots,f_{K} \in \Gamma_{0}(\mathcal{H})\) which are differentiable and \(\nu_{k}\)-Lipschitz.

Let also \( g_{1},\dots, g_{K} \in \Gamma_{0}(\mathcal{H})\) such that:
\begin{align}
  \label{eq:splif_func}
  \forall \x \in \mathcal{H},\, & f(\x) = \sum_{k=1}^{K}f_{k}(\x) \\
  \forall \x \in \mathcal{H},\, & g(\x) = \sum_{k=1}^{K}g_{k}(\x)
\end{align}
\end{Assumption}
\begin{Assumption}\label{ass:inter}
Let \(\nu_{1},\dots, \nu_{K} \in \mathbb{R}_{+}^{K}\).
Suppose there exists \(f_{1},\dots,f_{K} \in \Gamma_{0}(\mathcal{H})\) which are differentiable and \(\nu_{k}\)-Lipschitz.

Let also \( g_{1},\dots, g_{K} \in \Gamma_{0}(\mathcal{H})\) such that:
\[\cap_{k=1}^{K} \Zer\left\{\nabla f_k + \bm\partial g_{k}\right\} \neq \varnothing\]
\end{Assumption}

From \cref{ass:split},  we can then develop the online Forward-Backward Algorithm: at each step \(k\) the gradient of the data consistency term \(\nabla f\) is approximated by \(\nabla f_k\), and the regularisation term \(g_{k}\) is used. This idea of approximating the gradient term by a part of it, and can also be seen in the EM and MM class of algorithms, as well in the stochastic gradient descent widely in use in Machine Learning.
The online Forward-backard algorithm can be stated as follows, with \({(\eta_k)}_{\KK} > 0 \):
\begin{equation}
  \label{eq:online_fb}
  (\forall n \in \mathbb{N}),\quad
  \begin{cases}
    \x^{(n,k+1)} = \prox_{\eta_k g_{k}}\left(\x^{(n,k)}-\eta_k \nabla f_k(\x^{(n,k)})\right) & \forall k \in \{\KK\}\\
    \x^{(n+1,1)} = \x^{(n,K+1)}
\end{cases}
\end{equation}

Similarly to the offline forward-backward, its online version can also be expressed as a fixed point algorithm:

\begin{equation}
  \label{eq:online_fb_fixed_point}
  (\forall n \in \mathbb{N}),\quad
  \begin{cases}
  \x^{(n,k+1)} &= \bm{T}_k(\x^{(n,k)}) \quad (\forall k \in \{\KK\}),\\
  \x^{(n+1,1)} &= \x^{(n,K+1)}
\end{cases} \iff \x^{(n+1)} = \underbrace{\bm{T}^{K}\circ\dots\circ \bm{T}^{1}}_{\bm{\mathcal{T}}}\x^{(n)}
\end{equation}

Where, for every \(k \in \{1,\dots, K\}\), we have the fixed point operator of a step of the online algorithm,
\begin{equation}
  \bm{T}_k=  \prox_{\eta_k g_{k}}\left(Id -\eta_k \nabla  f_k\right)\label{eq:T_k}
\end{equation}
and the fixed point operator of the online algorithm:
\begin{equation}
\bm{\mathcal{T}} = \bm{T}_{K}\circ\dots\circ \bm{T}_{1} \\\label{eq:T_online}
\end{equation}

\paragraph{Remark}
Different choice  for \({(f_{k})}_{\KK}\) and \({(g_{k})}_{\KK}\) are proposed and studied in \cref{sec:application_cartesian} for the cartesian reconstruction, and in \cref{sec:application_non_cartesian} for the non cartesian case.

\section{Fixed point study for online and offline algorithms}%
\label{sec:fixed_point_algo}
From the definitions and theorems presented in \cref{sec:prop_fixed_point}, the following classical results on fixed point algorithm can be shown.
\begin{Lemma}\label{lemme:operator}
  \begin{itemize}
    \item Let \(\nu \in [0,+\infty [ \).
    \item Let \(f, g \in \Gamma_{0}(\mathcal{H})\).
    \item Let \(\eta \in \left[0, 2/\nu\right[\).
    \item Let \(\bm{A}= \bm{I} - \eta \nabla f \).
    \item Let \(\bm{B} = \prox_{\eta g}\).
  \end{itemize}
  Finally, let \(\bm{T} = \bm{B}\circ \bm{A}\)
 Suppose that \(\nabla f\) is \(\nu\)-Lipschitz, we have:
 \begin{enumerate}
    \item \(\bm{A}\) is \(\eta\nu/2\)-averaged (\cref{prop:baillon-haddad} and~\ref{prop:cocoercive-averaged})
    \item \(\bm{B}\) is \(1/2\)-averaged (\cref{prop:prox-averaged})
    \item \(\bm{T}\) is \(\alpha\)-averaged with \(\alpha = \frac{1}{2-\eta\nu/2}\) (\cref{corol:comp})
    \item \(\Fix \bm{T} = \Zer\{ \nabla f + \bm\partial g \}\)
    \item If \( \Zer\{ \nabla f \} \cap \Zer \{\bm\partial g\} \neq \varnothing \), \(\Fix \bm{T} = \Zer\{ \nabla f + \bm\partial g \} = \Zer\{ \nabla f \} \cap \Zer \{\bm\partial g\} \) (\cref{corol:comp})
 \end{enumerate}
\end{Lemma}

\begin{proof}
  Only (4.) is not directly shown from the previous propositions:
\begin{align}
    & \x \in \Fix \bm{T} \notag\\
    &   \Leftrightarrow  \x=\prox_{\eta g}(\x-\eta \nabla f(\x)) \notag \\
    &   \Leftrightarrow (\bm{I}-\eta \nabla f) \x \in(\bm{I} + \eta \bm{\partial} g) \x \\
    &   \Leftrightarrow  0 \in \nabla f(\x)+\bm\partial g(\x) \notag
  \end{align}
Consequently, \( \Fix \bm{T}=\Zer(\nabla f+\bm\partial g)=\Zer(\bm\partial(f+g))=\argmin(f+g)\)
\end{proof}
\begin{Proposition}[Fixed point for offline algorithm]\label{prop:fix_exist_offline}
  \begin{itemize}
    \item Let \(\nu \in [0,+\infty [ \)
  \item Let \(f, g \in \Gamma_{0}(\mathcal{H})\)
  \item Let \(\eta \in \left[0, 2/\nu\right[\)
  \end{itemize}
  Suppose that \(\nabla f\) is \(\nu\)-Lipschitz, the offline Forward-Backward algorithm \cref{eq:offline_fb} converged to a fixed point of \(\bm{T}\) the offline fixed-point operator~\eqref{eq:T_offline}.

  The offline algorithm converges thus to a solution of the offline problem~\eqref{eq:offline_fb}.
\end{Proposition}
\begin{proof}
  This is simply \cref{lemme:operator}, which then verifies the conditions of \cref{thm:bauschke}.
\end{proof}

\begin{Theorem}\label{prop:on2off}
  \begin{itemize}
    \item Let \(\nu > 0\), let \(f, g \in \Gamma_{0}(\mathcal{H})\), with \(\nabla f\) being \(\nu\)-Lipschitzian.
    \item Let \(\eta \in ] 0, 2/\nu [\).
    \item Let \({(\nu_k)}_{\KK}\) and \({(f_k)}_{\KK} \in \Gamma_{0}(\mathcal{H})\), such that \(\nabla f_k\) is \(\nu_k\)-Lipschitzian, and \(\eta_k\in ]0,2/\nu^{k}[ \)
    \item Let \({(g_k)}_{\KK} \in \Gamma_{0}(\mathcal{H})\)
  \end{itemize}
  Suppose that \cref{ass:split} and~\ref{ass:inter} holds. Then a fixed point of the online algorithm~\eqref{eq:online_fb} is also a fixed point of the offline one~\eqref{eq:offline_fb}:
  \begin{equation}
    \label{eq:inc_on_off}
    \Fix{\bm{\mathcal{T}}} \subset \Fix{\bm{T}} = \argmin{f+g}
  \end{equation}
\end{Theorem}

\begin{proof}
  The existence of a fixed point in \(\Fix \bm{\mathcal{T}}\) is guaranteed by \cref{prop:fix_exist_offline} and \cref{prop:comp_operator}. Recall \(\bm{\mathcal{T}}\) and \(\bm{T}\) the fixed point operator associated to the online~\eqref{eq:online_fb} and offline~\eqref{eq:offline_fb} algorithms.
  Let:
  \begin{align}
    \x \in \Fix \bm{\mathcal{T}} &= \cap_{k=1}^{K}\Fix\bm{T}_k\\
    \iff &   \forall k \in \{\KK\},\, \forall \y \in \mathcal{H},\,\left\langle \x-\y \middle| \nabla f_k(\x)\right\rangle + g_{k}(\x) \le g_{k}(\y) \\
    \implies &   \forall \y \in \mathcal{H},\,  \left\langle\x-\y \middle| \sum_{k=1}^{K} \nabla f_k(\x)\right\rangle + \sum_{k=1}^{K}g_{k}(\x) \le \sum_{k=1}^{K} g_{k}(\y) \\
    \implies &\forall \y \in \mathcal{H},\,  \left\langle \x-\y \middle| \nabla f(\x)\right\rangle + g(\x) \le g(\y) \\
                               & \iff \x \in \Fix \bm{T}
  \end{align}
\end{proof}


\paragraph{Remark}
\cref{ass:inter} is only a suffisant condition, if \(\cap_{k=1}^{K}\Fix \bm{T}_{k} \neq \varnothing\), \(\Fix \bm{\mathcal{T}}\) may still be non empty, and further study is then require to compare results of the online and offline algorithms, A variational approach developed first by Bertsekas~\cite{bertsekas_incremental_2011} is reproduced in appendix \cref{ann:onvar}.  Another approach, consider the case where the regularisation is applied after a cycle of evalution of the \((f_k)\), with example in the Machine learning litterature, this new type of online reconstruction (which could be call type III reconstruction) seems promising both in term of convergence conditions and results, but has however not been tested at the time of this report writing by lack of time.

\section{Application to Cartesian reconstruction}\label{sec:application_cartesian}
\subsection{Problem formulation}
In this chapter we develop a new paradigm of reconstruction, called online reconstruction, where the reconstruction is started simultaneously with the acquisition, and the newly acquired data is fed to the reconstruction algorithm incrementally. \cref{chap:mri_reco} presented the general framework of MRI reconstruction, for the sake of simplicity lets consider here the case of single coils reconstruction, using a Cartesian sampling scheme, in a analysis framework:
\begin{equation}
  \label{eq:mono_analysis}
    \hat{\x} = \arg\min_{\x\in \mathbb{C}^N}  \frac{1}{2}\left\|\bm{\Omega\F}  \x - \y \right\|_2^2 + R(\bm\Psi\, \x)
\end{equation}

Where \(\bm{\Om}\) is a subsampling mask, \(\F\) is the Fourier transform, \(\bm\Psi\) a sparsifify transform and \(R\) a regularisation function, promoting sparsity.
\subsubsection{Mask Operator Definition and properties}
Let \(K\) be the total number of shots acquired, and for every \(k \in \{1, \dots, K\}\), let \(p_k \in {\{1, N\}}^{\bar{p_{k}}}\) an ordered set of indices of the samples acquired in the k-space for the shot \(k\), with \(\bar{p}_k\) the number of samples collected. Finally let  \(\pi_k = \sum_{k'=1}^{K}\bar{p}^{k'}\).
Note that \(p_k\) thus represents an acquisition shot (or a batch of successive acquisitions), and typically,
From these elements we can define the various mask operations for each step \(k\):
\begin{align}
    \delta\bm\Omega_k &= \delta_{i,p_{ki}} \in \mathbb{C}^{\bar{p}_k\times N}\\
    \bm\Omega_k &= \begin{bmatrix}
     \delta\bm\Omega_1 &
     \dots &
     \delta\bm\Omega_k
   \end{bmatrix}^T  \in \mathbb{C}^{\pi_k\times N}\\
    \delta\y_k &\in \mathbb{C}^{\bar{p}_k}\\
    \y_k &= \begin{bmatrix}
      \delta\y_1 &
      \dots &
      \delta\y_k
      \end{bmatrix}^T
   \in \mathbb{C}^{\pi_k}
\end{align}
In particular we have, for every \(k \ge 2\):
\begin{equation}
   \bm\Omega_k = \begin{bmatrix}
     \bm\Omega_{k-1} &
     \delta\bm\Omega_k
    \end{bmatrix}^T \quad\text{and}\quad
   \y_k = \begin{bmatrix}
      \y_{k-1}&
      \delta\y_k
    \end{bmatrix}^T
\end{equation}

We also set the inplace mask operation in the Fourier space, defined as:
\begin{equation}
  \Om_k = {\bm\Omega_k}^{\top}\bm\Omega_k
\end{equation}
Furthermore the acquired data \(\y\) and the subsampling operator \(\bm\Omega\) have the following properties:

\begin{equation}
  \label{eq:omega_sel}
  (\forall k \in 1, \dots, K,) \quad
  \begin{cases}
    \delta \y_k= \delta\bm\Omega_k \y \\
    \y_k = \bm\Omega_k \y
  \end{cases}
\end{equation}
\begin{Proposition}\label{prop:omega_proj}
  \begin{itemize}
    \item In the case of Cartesian sampling, the inplace mask operator \(\Om_k\) (conversely \(\delta\Om_k\)), is a diagonal matrix of size \(N \times N\) with 1 or 0s on its diagonal. It is thus an orthogonal projector and a 1-lipschitzian operator.
    \item The set of mask operator \({(\delta\Om_k)}_{\KK}\)  forms an orthogonal family of projector \ie{}
          \begin{equation}
          \forall i, j , i \neq j \implies \delta\Om_{i}\circ\delta\Om_{j} = 0
        \end{equation}
  \end{itemize}
\end{Proposition}

\begin{Assumption}\label{ass:complete}
  The sequence \({(p_k)}_{1, \dots, K}\) performs a partition of a subset of \(\{1, \dots, N\}\) of size \(M\), \ie{}\({(p_k)}_{1,\dots, K}\) fully describes the sampling strategy of the k-space and \(\pi_k = M\).
\end{Assumption}

From \cref{ass:complete}, we also have \(\bm\Omega_{K} = \bm\Omega\), \ie{} the online reconstruction received the same data as the problem~\eqref{eq:mono_analysis}:

 \begin{equation}
\label{eq:online_ana}
    \hat{\x} = \arg\min_{\x\in \mathbb{C}^N} \underbrace{\frac{1}{2}\sum_{k=1}^K \left\|\delta\bm{\Omega}_k\left(\F\x - \y\right) \right\|_2^2}_{f(\x)} + \underbrace{R(\bm\Psi\, \x)}_{g(\x)}
 \end{equation}

\cref{eq:online_ana} is thus an application of~\eqref{eq:base_pb}. Moreover, the separability of the Cartesian Mask Operator calls for using an online reconstruction approach.
Here after we define thus the following function, that will later be used to approximate \(\nabla f\) the data

\begin{Proposition}\label{prop:lip_grad}
  Let \(\bm{\Omega}\) be a cartesian mask operator. The function:
  \begin{equation}
    \label{eq:f_ana}
    f(\x) =  \frac12 \left\|\bm{\Omega}\left(\F\x - \y\right) \right\|_2^2 \\
     \end{equation}
 have a gradient being \(\nu\)-Lipschitzian, with:
  \[ \nu = \||\F^{*}\Om\F|\| = 1 \]
\end{Proposition}

\begin{proof}
  First, let's recall that \(\F\) is the fourier operator, and as such is an orthogonal operator, this implies that \(\vertiii{\F}\vertiii{\F^{*}} = 1\).
  We have, for every \(k\in \{1, \dots, K\}\):
  \begin{align}
   \nabla f_k(\x) &=  \F^{*}\Om\left(\F\x - \y_{k} \right) \notag\\
 \forall \x,\x' \in \mathbb{C}^{N}, \quad \nabla f_k(\x) -  \nabla f_k(\x')& = \F^{*}\Om\F\left(\x - \x'\right) \notag\\
  \left\|\nabla f_k(\x) -  \nabla f_k(\x')\right\|_{2} &\le \vertiii{\F^{*}\Om\F} \left\|\x - \x'\right\|_{2} \notag\\
   \left\|\nabla f_k(\x) -  \nabla f_k(\x')\right\|_{2} &\le \vertiii{ \F^{*}}\cdot\vertiii{\Om}\cdot\vertiii{\F} \cdot \left\|\x - \x'\right\|_{2} \notag\\
   \left\|\nabla f_k(\x) -  \nabla f_k(\x')\right\|_{2} &\le \vertiii{\Om} \left\|\x - \x'\right\|_{2} \notag\\
   \left\|\nabla f_k(\x) -  \nabla f_k(\x')\right\|_{2} &\le \left\|\x - \x'\right\|_{2}\label{eq:grad_lipschitz}
  \end{align}
  As \(\Om\) is an orthogonal projector (\cref{prop:omega_proj}), we have \(\vertiii{\Om} = 1\),
\end{proof}

\subsection{Result for Forward Backward reconstruction}

To solve the problem~\eqref{eq:mono_analysis}, the Forward-Backward algorithm and its online variants can be used. In particular, we show

\paragraph{Offline Reconstruction}
For the offline reconstruction algorithm~\eqref{eq:offline_fb}, recall that \(f\) has a 1-Lipschitzian gradient (\cref{prop:lip_grad}). According to \cref{prop:fix_exist_offline}, the Forward-backward algorithm associated~\eqref{eq:offline_fb} converges toward of a solution of the offline problem: \cref{eq:mono_analysis}.


\paragraph{Online Reconstruction type I}

The first approach to an online reconstruction which have been tested, was to incrementaly load the data at each step and solve the problem for this incomplete data, as presented in the chapter 5 of Loubda El Gueddari PhD Thesis~\cite{gueddari_proximal_2019}.
For instance let:
\begin{equation}
  \label{eq:F_k}
  F_{k}(\x) = \frac{1}{2} \sum_{j=1}^{k} \left\|\delta\bm{\Omega}_j\left(\F\x - \y\right) \right\|_2^2  \\
\end{equation}

Then algorithm~\eqref{eq:offline_fb} is applied with \(f_{k}=F_{k}\). This method does not verify \cref{ass:split}. However, if the masking operation is separable between each step, and converges to the offline mask (\ie{} \cref{ass:complete} holds), then we have, for every \(k \in \{\KK\}\)
\begin{itemize}
  \item \(F_{k}\) is 1- lipschitz (\cref{prop:lip_grad}),
  \item \(\Ker F_{k+1} \subset \Ker F_{k}\) and \(\Img F_{k} \subset \Img F_{k+1}\)
\end{itemize}

In particular this enforce that \(Fix T_{k+1} \subset \Fix T_{k}\), and the convergence towards the offline reconstruction is thus guaranteed.

\paragraph{Online Reconstruction type II}

To use the online Forward-Backward algorithn \eqref{eq:online_fb},  the  offline problem~\eqref{eq:mono_analysis} needs to be reformulated such that it verifies \cref{ass:split}.
For instance, lets:
\begin{align*}
  f(\x) &= \frac{1}{2}\left\|\bm{\Omega}\left(\F\x - \y\right) \right\|_2^2 =\frac{1}{2}\sum_{k=1}^K \left\|\delta\bm{\Omega}_k\left(\F\x - \y\right) \right\|_2^2 \\
  g(\x) &= R(\bm{\Psi\,\x})
\end{align*}
for every \(k \in \{\KK\}\) set:
\begin{align}
  f_{k}(\x) &= \frac{1}{2} \left\|\delta\bm{\Omega}_k\left(\F\x - \y\right) \right\|_2^2  \\
  g_{k}(\x) &= \frac{1}{K} g(\x)
\end{align}
According to \cref{prop:lip_grad}, \(f_{k}\) is \(1\)-Lipschitzian. \(f_{k}+g_{k}\) is a proper  convex function, so \(\Zer\{\nabla f_{k}+\bm\partial g_{k}\}\) is  non empty. If we assume \(\cap_{k=1}^{K}\Zer\{\nabla f_{k}+\bm\partial g_{k}\} \neq \varnothing\), \cref{ass:split} is verified, and \cref{prop:on2off} holds. The online mri reconstruction problem will provide a solution to the offline reconstruction.

\subsection{Extension to Synthesis formulation}
In the synthesis formulation the reconstruction is performed in the sparse domain (\eg{} Wavelet domain). This yields the following formulations:
\begin{equation}
  \label{eq:mono_synthesis}
  \hat{\bm\alpha} = \arg\min_{\bm\alpha\in \mathbb{C}^{N_{\bm\Psi}}} \frac{1}{2}\left\|\bm{\Omega}\F\bm\Psi^{*}\bm\alpha - \y \right\|_2^2 +R(\bm\alpha) \text{ and } \x = \bm{\Psi}^{*} \bm{\alpha}
\end{equation}
And similarly to~\eqref{eq:mono_analysis}, we have
\begin{align*}
  f(\bm{\alpha}) &= \frac{1}{2}\left\|\bm{\Omega}\left(\F\bm\Psi^{*}\bm{\alpha} - \y\right) \right\|_2^2 =\frac{1}{2}\sum_{k=1}^K \left\|\delta\bm{\Omega}_k\left(\F\bm{\Psi}^{*}\bm{\alpha} - \y\right) \right\|_2^2 \\
  g(\bm{\alpha}) &= R(\bm\alpha)
\end{align*}
 and to verify \cref{ass:split},  for every \(k \in \{\KK\}\):
\begin{align}
  f_{k}(\bm{\alpha}) &= \frac{1}{2} \left\|\delta\bm{\Omega}_k\left(\F\bm{\Psi}^{*}\bm{\alpha} - \y\right) \right\|_2^2  \\
  g_{k}(\bm{\alpha}) &= \frac{1}{K} g(\bm{\alpha})
\end{align}
In the context of synthesis formulation, \(\nu_{k}\) the lipschitz constant of \(f_{k}\) can be determined, similarly to \cref{prop:lip_grad}, as:
\begin{equation}
\nu_{k} =  \vertiii{\Psi\F^{*}\Om\F\Psi^{*}} = \vertiii{\Psi}\vertiii{\Psi^{*}} \dots \nu_{ana,k} = \vertiii{\Psi}\vertiii{\Psi^{*}}
\end{equation}
Where \(\nu_{ana,k}\) is the lipschitz constant in the analysis case, equal to 1 according to \cref{prop:lip_grad}.

\paragraph{Remark}If \(\bm\Psi\) is a tight frame operator, then we also have \(\nu_{k} = 1\), and the synthesis formulation is equivalent to the analysis one. Otherwise, the frame operator introduces a bias that needs to be compensated.

\subsection{Extension to parallel Imaging}
In the case of parallel imaging, the mask operator and the fourier transform are repeated along the coils:
\begin{equation}
  \label{eq:analysis_p}
    \hat{\ubm{x}} = \argmin_{\ubm{x}\in \mathbb{C}^{L\times N}} \left\{ \frac12 \sum_{\ell=1}^{L} \left\| \bm{\Omega}\F \x_{\ell}) -\y_{\ell} \right\|_2^2 + g(\ubm{\Psi}\,\ubm{x})\right\}
  \end{equation}
Idem, set:
\begin{align*}
  f(\ubm{x}) &= \frac{1}{2}\sum_{\ell=1}^{L}\left\|\bm{\Omega}\left(\F\x_{\ell} - \y\right) \right\|_2^2 =\frac{1}{2}\sum_{k=1}^K \sum_{\ell=1}^{L}\left\|\delta\bm{\Omega}_k\left(\F\x_{\ell} - \y\right) \right\|_2^2 \\
  g(\ubm{x}) &= R(\ubm{\Psi}\,\ubm{x})
\end{align*}
and for every \(k \in \{\KK\}\):
\begin{align}
  f_{k}(\ubm{x}) &= \frac{1}{2} \sum_{\ell=1}^{L} \left\|\delta\bm{\Omega}_{\ell,k}\left(\F\ubm{x} - \y\right) \right\|_2^2\\
  g_{k}(\ubm{x}) &= \frac{1}{K} g(\ubm{x})
\end{align}

The  Lipschitz constant of  \(f_{k}\) is \(\nu_{k} = L\).



% \section{Application to Non Cartesian reconstruction}%
% \label{sec:application_non_cartesian}


% To further reduce the acquisition time, the compress sensing theory calls for a non cartesian sampling. However it greatly increase the cost of the reconstruction, and the problem needs to be reformulated for the case where a k-space point is sampled multiple time, with a possible different value at each sampling. Sampling the same point in the kspace can be notably use to compensate the movement of the sampled object, and in the case of the center point of the k-space, it is used to perfom density compensation.


\end{document}

%
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "english"
%%% End:
